#include "CTS_Layer.h"
#include "structure.h"
#include "msp.h"

void init(void);
void initTouch(void);
void initDelay(void);
void delay(void);
void updateLED(struct Element *);


int main(void)
{
    uint8_t ledUpdate;
    init();

    while (1)
    {
        ledUpdate = BIT0;
        P1->OUT &= ~ledUpdate;

        if(TI_CAPT_Button(&button1)) ledUpdate |= BIT7;

        P1->OUT |= ledUpdate;

        delay();
    }
}

void init(void)
{
    WDTCTL = WDTPW + WDTHOLD;
    initDevice();
    initTouch();
}

void initTouch(void)
{
    TI_CAPT_Init_Baseline(&button1);
    TI_CAPT_Update_Baseline(&button1, 5);
}

void initDevice(void)
{
    P1->SEL0 &= ~BIT0;
    P1->SEL1 &= ~BIT0;
    P1->DIR |= BIT0;
    P2->OUT |= BIT0;
}

void delay(void)
{
    __delay_cycles(100);
}

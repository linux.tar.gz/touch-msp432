#include "structure.h"

const struct Element element0 = {
    .inputBits = CAPTIOPOSEL_2 + CAPTIOPOSEL_7,
    .threshold = 100,
    .maxResponse = 200
};

// const struct Sensor button = {
//     .halDefinition = RO_CSIO_TA2_TA3,
//     .inputCapsioctlRegister = (uint16_t * )  &CAPTIO0CTL,
//     .numElements = 1,
//     .baseOffset = 0,
//     .arrayPtr[0] = &element,
//     .measGateSource = GATE_WDTA_SMCLK,
//     .accumulationCycles = WDTA_GATE_64
// };


const struct Sensor button1 =
{
    .halDefinition = RO_CSIO_TA2_TA3,
    .inputCapsioctlRegister = (uint16_t *)&CAPTIO0CTL,
    .numElements = 6,
    .baseOffset = 0,
    .arrayPtr[0] = &element0,
    .measGateSource = TIMER_SMCLK,
    .sourceScale = TIMER_SOURCE_DIV_0,
    .accumulationCycles = 50
};
